﻿using SOLID.Abstractions;

namespace SOLID.Services
{
    internal class StartGameService:IStartGameService
    {
        private readonly IEnumerable<IGameService> _gameServices;
        private bool _isStarted = true;
        private readonly IConfiguration _configuration;
        public StartGameService( IConfiguration configuration, IEnumerable<IGameService> gameServices)
        {
            _configuration = configuration;
            _gameServices = gameServices;
        }
        public void StartGame()
        {
            while (_isStarted)
            {
                Console.WriteLine("Выберите команду, введите значение от 0 до 2:\n" +
                    "0. Exit\n" +
                    "1. Начать игру\n");

                var command = Console.ReadLine();
                Console.WriteLine("-------------------------------------------");

                switch (command)
                {
                    case "0": _isStarted = false; break;
                    case "1": SelectLevel(); break;
                    default: Console.WriteLine("Неизвестная команда\n"); break;
                }
            }
        }
        private void SelectLevel()
        {
            var level = _configuration.GetSection("level").Value;
            bool isLevelHard = level == "hard";
            bool isLevelMiddle = level == "middle";
            bool isLevelEasy = level == "easy";

            foreach (var serv in _gameServices)
            {
                if (isLevelEasy && serv.GetType() == typeof(EasyGameService))
                {
                    PrintMessage(level);
                    serv.Start();
                    break;
                }
                if (isLevelMiddle && serv.GetType() == typeof(MiddleGameService))
                {
                    PrintMessage(level);
                    serv.Start();
                    break;
                }
                if (isLevelHard && serv.GetType() == typeof(HardGameService))
                {
                    PrintMessage(level);
                    serv.Start();
                    break;
                }
                else if(!isLevelEasy&& !isLevelMiddle && !isLevelHard)
                {
                    throw new ArgumentException("Ошибка настроек, исправьте и перезапустите игру");
                }
            }
        }
        private void PrintMessage(string level)
        {
            Console.WriteLine("Добро пожаловать в игру !!!\n"+
                $"Выбран уровень сложности {level} \n");
        }
    }
}
