﻿using SOLID.Abstractions;

namespace SOLID.Services
{
    internal class GenerateService :IGenerateService
    {
        public int Generate(int minValue , int maxValue)
        {
            return new Random().Next(minValue, maxValue);
        }
    }
}
