﻿
using SOLID.Abstractions;

namespace SOLID.Services
{
    internal class HardGameService : IGameService, IResultGame, IChoiceEvaluation
    {
        private readonly IGenerateService _generateService;
        private readonly IConfiguration _configuration;
        public HardGameService(IGenerateService generateService, IConfiguration configuration)
        {
            _generateService = generateService;
            _configuration = configuration;
        }
        public void Start()
        {
            int _attempt = 0;

            var tokenSource = new CancellationTokenSource();
            TimeSpan timeSpan = TimeSpan.FromMinutes(1);
            tokenSource.CancelAfter(timeSpan);
            CancellationToken token = tokenSource.Token;    

            var minValue = _configuration.GetValue<int>("range:minValue");
            var maxValue = _configuration.GetValue<int>("range:maxValue");
            if (minValue > maxValue)
            {
                throw new ArgumentException("Ошибка настроек, исправьте и перезапустите игру");
            }

            var number = _generateService.Generate(minValue, maxValue);
            var attempts = _configuration.GetValue<int>("attempts");

            PrintRules(minValue, maxValue, attempts, int.Parse(timeSpan.Minutes.ToString()));

            while (_attempt < attempts && token.IsCancellationRequested ==false)
            {
                Console.WriteLine("Введите число:\n");
                var line = Console.ReadLine();
                bool succes = int.TryParse(line, out int value);

                _attempt++;
                if (succes)
                {
                    if (value == number)
                    {
                        Win();
                        break;
                    }
                    else if (value > number)
                    {
                        PrintMore(value);
                        PrintCountAttemts(attempts - _attempt);
                    }
                    else
                    {
                        PrintLess(value);
                        PrintCountAttemts(attempts - _attempt);
                    }
                }
            }
            if (_attempt == attempts)
            {
                Console.WriteLine("Исчерпано число попыток");
            }
            else if (token.IsCancellationRequested == true)
            {
                Lose();
            }
        }

        public void PrintRules(int min, int max, int attemts, int timer)
        {
            Console.WriteLine("Правила игры: \n" +
                $"1. Угадать число в промежутке от {min} до {max} \n" +
                $"2. Количество попыток {attemts}\n" +
                $"3. Время за которое нужно отгадать {timer}\n"+
                "Время пошло!!!\n");
        }
        public void PrintCountAttemts(int attempts) => Console.WriteLine($"Осталось попыток {attempts}\n");
        public void Win() => Console.WriteLine("Поздравляем вы угадали число\n");
        public void Lose() => Console.WriteLine("Время истекло, желаю удачи в следующий раз\n");
        public void PrintLess(int value) => Console.WriteLine($"Число {value} меньше загаданного числа\n");
        public void PrintMore(int value) => Console.WriteLine($"Число {value} больше загаданного числа\n");
    }
}
