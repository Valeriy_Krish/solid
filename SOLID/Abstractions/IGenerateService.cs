﻿
namespace SOLID.Abstractions
{
    internal interface IGenerateService
    {
        public int Generate(int munVal , int maxVal);
    }
}
