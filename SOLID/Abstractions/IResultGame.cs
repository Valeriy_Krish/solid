﻿
namespace SOLID.Abstractions
{
    internal interface IResultGame :IConsoleService
    {
        void Win();
        void Lose();
    }
}
