﻿
namespace SOLID.Abstractions
{
    internal interface IChoiceEvaluation : IConsoleService
    {
        void PrintLess(int value);
        void PrintMore(int value);
    }
}
