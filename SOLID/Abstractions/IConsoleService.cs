﻿
namespace SOLID.Abstractions
{
    internal interface IConsoleService
    {
        void PrintRules(int min, int max, int attemts, int timer);
        void PrintCountAttemts(int attemts);
     /*   void Win();
        void Lose();*/
/*        void PrintLess(int value);
        void PrintMore(int value);*/
    }
}
