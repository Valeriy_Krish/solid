﻿
namespace SOLID.Abstractions
{
    internal interface IGameService
    {
        void Start();
    }
}
