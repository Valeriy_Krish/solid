﻿
namespace SOLID.Abstractions
{
    internal interface IStartGameService
    {
        void StartGame();
    }
}
