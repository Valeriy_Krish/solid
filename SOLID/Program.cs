﻿using SOLID.Abstractions;
using SOLID.Services;

namespace SOLID;

static class Program
{
    static void Main(string[] args)
    {
        var builder = new ConfigurationBuilder().AddJsonFile("config.json");
        IConfiguration AppConfiguration = builder.Build();

        var service = new ServiceCollection()
            .AddSingleton(AppConfiguration)
            .AddSingleton<IGameService, EasyGameService>()
            .AddSingleton<IGameService, HardGameService>()
            .AddSingleton<IGameService, MiddleGameService>()
            .AddSingleton<IGenerateService, GenerateService>()
            .AddSingleton<IStartGameService, StartGameService>()
            .BuildServiceProvider();

        var start = service.GetService<IStartGameService>();
        start?.StartGame();

    }
}

