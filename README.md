# SOLID

## Принцип разделения интерфейсов

- IConsoleService

## Принцип инверсия зависимостей 

- IGameService

## Принцип открытости/закрытости

- IGameService , IStartGameService